class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :answers do |t|
      t.string :description
      t.boolean :value
      t.boolean :selected

      t.timestamps
    end
  end
end
