# == Schema Information
#
# Table name: question_sub_types
#
#  id               :bigint(8)        not null, primary key
#  code             :string
#  description      :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  question_type_id :bigint(8)
#
# Indexes
#
#  index_question_sub_types_on_question_type_id  (question_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (question_type_id => question_types.id)
#

class QuestionSubType < ApplicationRecord
  # belongs_to = "pertence a"
  # lê-se: "o campo :question_type pentence a outra tabela"
  # É obrigatorio preecher esse campo
  belongs_to :question_type
  has_many :questions

  extend Enumerize
  enumerize :description, in: [:first_name, :last_name,
                               :district_name,
                               :birth_day, :birth_month, :birth_year,
                               :first_name_mother, :last_name_mother],
            predicates: true
end
