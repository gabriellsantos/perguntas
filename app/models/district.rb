# == Schema Information
#
# Table name: districts
#
#  id          :bigint(8)        not null, primary key
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class District < ApplicationRecord
end
