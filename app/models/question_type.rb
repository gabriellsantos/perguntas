# == Schema Information
#
# Table name: question_types
#
#  id          :bigint(8)        not null, primary key
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class QuestionType < ApplicationRecord
  has_many :questions
  has_many :question_sub_types

  extend Enumerize
  enumerize :description, in: [:name, :district, :date_of_birth, :county, :name_mother], predicates: true

end
