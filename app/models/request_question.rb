# == Schema Information
#
# Table name: request_questions
#
#  id              :bigint(8)        not null, primary key
#  description     :string
#  value           :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  question_id     :bigint(8)
#  user_request_id :bigint(8)
#
# Indexes
#
#  index_request_questions_on_question_id      (question_id)
#  index_request_questions_on_user_request_id  (user_request_id)
#
# Foreign Keys
#
#  fk_rails_...  (question_id => questions.id)
#  fk_rails_...  (user_request_id => user_requests.id)
#

class RequestQuestion < ApplicationRecord
  belongs_to :question
  belongs_to :user_request
  has_many :answers

  def day_rand(day_except)
    ([*1..30] - [day_except.to_i])
  end

  def month_rand(month_except)
    ([*1..12] - [month_except.to_i])
  end

  def year_rand(year_except)
    ([*1945..2000] - [year_except.to_i])
  end

  after_create :generate_answers

  def generate_answers
    # Todos os tipos de pergunta
    name_type = QuestionType.where(description: :name).take!
    date_of_birth_type = QuestionType.where(description: :date_of_birth).take!
    name_mother_type = QuestionType.where(description: :name_mother).take!

    jsonb_result = ActiveSupport::JSON.decode(self.user_request.jsonb_result).with_indifferent_access

    # Se a question for do tipo: Nome
    if self.question.question_type == name_type

      name_true = jsonb_result[:nome]

      case self.question.question_sub_type.description

      when :first_name.to_s
        for i in 1..3
          Answer.create(description: Faker::Name.unique.first_name.upcase, value: false, selected: false, request_question: self)
        end
        # Criar uma resposta verdadeira
        Answer.create(description: name_true.truncate_words(1, omission: '').upcase, value: true, selected: false, request_question: self)

      when :last_name.to_s
        for i in 1..3
          Answer.create(description: Faker::Name.unique.last_name.upcase, value: false, selected: false, request_question: self)
        end
        # Criar uma resposta verdadeira
        index_last_space = name_true.rindex(' ') + 1
        size_name = name_true.size
        Answer.create(description: name_true[index_last_space, size_name].upcase, value: true, selected: false, request_question: self)
      end

    end

    # Se quer respostas do tipo: Data de Nascimento
    if self.question.question_type == date_of_birth_type
      # Busca dados no banco
      date_of_birth_true = jsonb_result[:data_nascimento]
      date_of_birth_true = date_of_birth_true.to_s

      day_true = date_of_birth_true[6..7]
      month_true = date_of_birth_true[4..5]
      year_true = date_of_birth_true[0..3]

      days = day_rand(day_true)
      months = month_rand(month_true)
      years = year_rand(year_true)

      # Verificando os sub tipos de perguntas
      case self.question.question_sub_type.description
      when :birth_day.to_s
        for i in 1..3
          day = days.sample
          days = days.reject {|a| a == day}
          Answer.create(description: "#{day < 10 ? "0#{day}" : day }", value: false, selected: false, request_question: self)
        end
        # Coloca uma resposta verdadeira
        Answer.create(description: day_true, value: true, selected: false, request_question: self)

      when :birth_month.to_s
        for i in 1..3
          month = months.sample
          months = months.reject {|a| a == month}
          Answer.create(description: "#{month < 10 ? "0#{month}" : month }", value: false, selected: false, request_question: self)
        end
        # Coloca uma resposta verdadeira
        Answer.create(description: month_true, value: true, selected: false, request_question: self)

      when :birth_year.to_s
        for i in 1..3
          year = years.sample
          years = years.reject {|a| a == year}
          Answer.create(description: year, value: false, selected: false, request_question: self)
        end
        # Coloca uma resposta verdadeira
        Answer.create(description: year_true, value: true, selected: false, request_question: self)
      end
    end

    # Se a question for do tipo: Nome da Mae
    if self.question.question_type == name_mother_type



      name_mother_true = jsonb_result[:nome_mae]

      case self.question.question_sub_type.description

      when :first_name_mother.to_s
        for i in 1..3
          Answer.create(description: Faker::Name.unique.female_first_name.upcase, value: false, selected: false, request_question: self)
        end
        # Criar uma resposta verdadeira
        Answer.create(description: name_mother_true.truncate_words(1, omission: '').upcase, value: true, selected: false, request_question: self)

      when :last_name_mother.to_s
        for i in 1..3
          Answer.create(description: Faker::Name.unique.last_name.upcase, value: false, selected: false, request_question: self)
        end
        # Criar uma resposta verdadeira
        index_last_space = name_mother_true.rindex(' ') + 1
        size_name = name_mother_true.size
        Answer.create(description: name_mother_true[index_last_space, size_name].upcase, value: true, selected: false, request_question: self)
      end

    end

  end
end
