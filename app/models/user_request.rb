# == Schema Information
#
# Table name: user_requests
#
#  id                  :bigint(8)        not null, primary key
#  cpf                 :string
#  json_result         :string
#  jsonb_result        :text             default("{}")
#  registration_status :string
#  return_web_service  :boolean
#  token               :string
#  value               :boolean
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class UserRequest < ApplicationRecord
  has_many :request_questions

  extend Enumerize
  enumerize :registration_status, in: [:created, :registrated, :expired], predicates: true, default: :created

  # gera o token: begore
  has_secure_token

  after_create :generate_request_questions

  def self.can_authenticate_in_the_moment(cpf)

    if CPF.valid? cpf
      limit_label = '40 minutos'
      limit = 40.minute.ago

      user = UserRequest.where(["created_at > ? and cpf = ?", limit, "#{cpf}"]).last

      if user == nil
        # Nenhuma tentativa desse usuario ate o momento
        return true
      else
        creation_time = user.created_at

        if creation_time > limit
          # Ainda não passou o prazo de espera obrigatorio
          return "Ainda não se passaram #{limit_label} desde sua última tentativa. Fantam #{UserRequest.time_diff(creation_time, limit)}"
        else
          # Passou o prazo de espera obrigatorio
          return true
        end
      end
    else
      return "este cpf [#{cpf}] não é válido"
    end

  end

  def self.time_diff(start_time, end_time)
    seconds_diff = (start_time - end_time).to_i.abs

    hours = seconds_diff / 3600
    seconds_diff -= hours * 3600

    minutes = seconds_diff / 60
    seconds_diff -= minutes * 60

    seconds = seconds_diff

    "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
  end

  def self.search_cpf(cpf)
    number_server = rand(1..3)
    client = Savon.client(wsdl: "http://jbossslave0#{number_server}:8081/frrp-ws-intranet/servlet/aws_infoconv_proxy?WSDL")
    response = client.call(:execute, xml: "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:fron='Fronteira_Rapida_PDC'>
         <soapenv:Header/>
         <soapenv:Body>
            <fron:Ws_InfoConv_Proxy.Execute>
               <fron:Listadecpf>#{cpf}</fron:Listadecpf>
               <fron:Cpfusuario>06250631127</fron:Cpfusuario>
            </fron:Ws_InfoConv_Proxy.Execute>
         </soapenv:Body>
      </soapenv:Envelope>")
  end

  def generate_request_questions

    # criar as perguntas da requisição
    if self.return_web_service
      # Busco todas perguntas
      questions = Question.all.shuffle
      #questions =  Question.where(question_type: QuestionType.where(description: :date_of_birth).first)

      for i in 1..3
        question = questions.sample
        questions = questions.reject {|a| a == question} # rejeito as questoes que ja usei
        # Crio pergunta baseado na requisicao do usuario
        RequestQuestion.create(description: question.description, question: question, user_request: self, value: false)
      end

    end
  end
end
