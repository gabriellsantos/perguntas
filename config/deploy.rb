require "bundler/capistrano"
require "rvm/capistrano"
require "rvm/capistrano/gem_install_uninstall"

#server "10.74.0.112", :web, :app, :db, primary: true
server "10.48.252.23", :web, :app, :db, primary: true
set :application, "perguntas"
set :user, "sistemas"
set :port, 22
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, "git"
set :repository, "http://git.seplan.to.gov.br/gds/perguntas_cidadao.git"
set :branch, "master"


default_run_options[:pty] = true
ssh_options[:forward_agent] = false

after "deploy", "deploy:cleanup" # keep only the last 5 releases
after "deploy", "deploy:migrate"

namespace :deploy do
  %w[start stop restart].each do |command|
    desc "#{command} unicorn server"
    task command, roles: :app, except: {no_release: true} do
      run "sh #{current_path}/config/unicorn_init.sh #{command}"
    end
  end

  task :restart_all, roles: :app, except: {no_release: true} do

    %w[macrozee agenda conselhos eventos monitora notificato piloto sms  gestores oquena ponto prg redmine toauth perguntas gastos].each do |pa|
      run "sh /home/sistemas/apps/#{pa}/current/config/unicorn_init.sh restart unicorn server"
    end
  end

  task :up_all_unicorn, roles: :app do
   list =  Dir.entries('/home/sistemas/apps').select {|entry| File.directory? File.join('/your_dir',entry) and !(entry =='.' || entry == '..') }
  end


  task :setup_config, roles: :app do
    sudo "ln -nfs #{current_path}/config/nginx.conf /etc/nginx/sites-enabled/#{application}"
    sudo "ln -nfs #{current_path}/config/unicorn_init.sh /etc/init.d/unicorn_#{application}"
    run "mkdir -p #{shared_path}/config"
    put File.read("config/database.yml"), "#{shared_path}/config/database.yml"
    run "mkdir -p #{shared_path}/public/uploads"
  end
  after "deploy:setup", "deploy:setup_config"

  task :define_init, roles: :app do
    #inicializacao
    sudo "ln -nfs #{current_path}/config/unicorn_init.sh /etc/rc5.d/S20unicorn_#{application}"
    sudo "ln -nfs #{current_path}/config/unicorn_#{application}.service /lib/systemd/system/unicorn_#{application}.service"
    sudo "chmod +x #{current_path}/config/unicorn_#{application}.service"
    sudo "chmod +x #{current_path}/config/unicorn_init.sh"
    #
  end
  after "deploy:define_init", "deploy:define_init"

  task :symlink_config, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/public/uploads #{release_path}/public/uploads"
    #run "ln -nfs #{current_path}/vendor/assets/ #{current_path}/public/assets/"
  end

  after "deploy:finalize_update", "deploy:symlink_config"

  desc " Verifica se a versão do projeto local é a mesma do repositório remoto"
  task :check_revision, roles: :web do
    unless `git rev-parse HEAD` == `git rev-parse origin/master`
      puts "WARNING: HEAD is not the same as origin/master"
      puts "Execute `git push` para sincronizar as mudanças."
      exit
    end
  end
  before "deploy", "deploy:check_revision"
  #after "deploy", "backup"
end

desc "tailf production log files"
task :log, :roles => :app do
  if(ARGV.index('log'))
    ARGV.values_at(Range.new(ARGV.index('log')+1,-1)).each do |log|
      trap("INT") { puts 'Interupted'; exit 0; }
      run "tail -f #{shared_path}/log/#{log}.log" do |channel, stream, data|
        puts "\n#{channel[:host]}: #{data}"
        break if stream == :err
    end
  end
    exit(0)
  end
end


desc "list log files"
task :logs, :roles => :app do
  run "ls -la #{shared_path}/log/"
end


desc "reload database migrate"
task :reset_db, :roles => :app do
  #run "cd #{current_path} && bundle exec rake RAILS_ENV=production  db:fixtures:dump"
  puts "Essa operação foi suspensa para impedir problemas, para reativa-la descomente no arquivo deploy"
  #run "cd #{current_path} && bundle exec rake RAILS_ENV=production  db:migrate VERSION=0"
  #run "cd #{current_path} && bundle exec rake RAILS_ENV=production  db:migrate"
end

desc "Realize backup da base de dados"
task :backup, :roles => :app do
  run "cd #{current_path} && bundle exec rake RAILS_ENV=production  db:fixtures:dump"
end

desc "bundle list"
task :list_gems, :roles => :app do
  run "cd #{current_path} && RAILS_ENV=production bundle list "
end


#BLOCO EXECUTE COMMANDS
set :sudo_call, ''
desc 'makes remote/rake calls to be executed with sudo'
task :use_sudo do
  set :sudo_call, 'sudo'
end

desc 'cap sake db:seed'
task :sake do
  if(ARGV.index('sake'))
    ARGV.values_at(Range.new(ARGV.index('sake')+1,-1)).each do |task|
      run "cd #{current_path}; #{sudo_call} RAILS_ENV=production bundle exec rake #{task}"
    end
    exit(0)
  end
end

desc 'run remote command'
task :remote do
  if(ARGV.index('remote'))
    command=ARGV.values_at(Range.new(ARGV.index('remote')+1,-1))
    run "cd #{current_path}; #{sudo_call} RAILS_ENV=production #{command*' '}"
    exit(0)
  end
end

desc 'run specified rails code on server'
task :runner do
  if(ARGV.index('runner'))
    command=ARGV.values_at(Range.new(ARGV.index('runner')+1,-1))
    run "cd #{current_path}; RAILS_ENV=production rails runner '#{command*' '}'"
    exit(0)
  end
end
#BLOCO EXECUTE COMMANDS
