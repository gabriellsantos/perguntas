namespace :dev do

  def show_sppiner(msg_start, msg_end = "Concluído!")
    spinner = TTY::Spinner.new("[:spinner] #{msg_start}...")
    spinner.auto_spin
    yield
    spinner.success("(#{msg_end})")
  end

  desc 'Apaga, cria, migra e popula banco de dados'
  task drop_and_set_db: :environment do
      show_sppiner("Apagando BD...") do
        %x{rails db:drop}
      end

      show_sppiner("Criando BD...") do
        %x{rails db:create}
      end

      show_sppiner("Migrando BD...") do
        %x{rails db:migrate}
      end

      %x{rails dev:popula_db}
  end

  desc 'Popula banco de dados'
  task popula_db: :environment do
      # Popula ---------
      show_sppiner("Adicionando QuestionTypes...") do
        %x{rails dev:add_question_types}
      end

      show_sppiner("Adicionando QuestionSubTypes...") do
        %x{rails dev:add_question_sub_types}
      end

      show_sppiner("Adicionando Questions...") do
        %x{rails dev:add_questions}
      end
  end

  desc 'Cadastro de QuestionTypes'
  task add_question_types: :environment do
    question_types = [
        {
            description: :name
        },
        {
            description: :date_of_birth
        },
        {
            description: :name_mother
        }
    ]
    question_types.each do |question_type|
      QuestionType.find_or_create_by!(question_type)
    end
  end

  desc 'Cadastro de QuestionSubTypes'
  task add_question_sub_types: :environment do
    question_sub_types = [
        {
            description: :first_name,
            question_type: QuestionType.where(description: :name).take!,
            code: '1'
        },
        {
            description: :last_name,
            question_type: QuestionType.where(description: :name).take!,
            code: '3'
        },
        {
            description: :birth_day,
            question_type: QuestionType.where(description: :date_of_birth).take!,
            code: '1'
        },
        {
            description: :birth_month,
            question_type: QuestionType.where(description: :date_of_birth).take!,
            code: '2'
        },
        {
            description: :birth_year,
            question_type: QuestionType.where(description: :date_of_birth).take!,
            code: '3'
        },
        {
            description: :first_name_mother,
            question_type: QuestionType.where(description: :name_mother).take!,
            code: '1'
        },
        {
            description: :last_name_mother,
            question_type: QuestionType.where(description: :name_mother).take!,
            code: '2'
        }
    ]
    question_sub_types.each do |question_sub_type|
      QuestionSubType.find_or_create_by!(question_sub_type)
    end
  end

  desc 'Cadastro de Questions'
  task add_questions: :environment do

    # tipos de pergunta
    name_type = QuestionType.where(description: :name).take!
    date_of_birth_type = QuestionType.where(description: :date_of_birth).take!
    name_mother_type = QuestionType.where(description: :name_mother).take!

    questions = [
        {
            description: 'Qual o seu primeiro nome?',
            question_type: name_type,
            question_sub_type: QuestionSubType.where(question_type: name_type, code: '1').take!
        },
        {
            description: 'Qual o seu último nome?',
            question_type: name_type,
            question_sub_type: QuestionSubType.where(question_type: name_type, code: '3').take!
        },
        {
            description: 'Qual o dia do seu aniversário?',
            question_type: date_of_birth_type,
            question_sub_type: QuestionSubType.where(question_type: date_of_birth_type, code: '1').take!
        },
        {
            description: 'Em qual mês você nasceu?',
            question_type: date_of_birth_type,
            question_sub_type: QuestionSubType.where(question_type: date_of_birth_type, code: '2').take!
        },
        {
            description: 'Em qual ano você nasceu?',
            question_type: date_of_birth_type,
            question_sub_type: QuestionSubType.where(question_type: date_of_birth_type, code: '3').take!
        },
        {
            description: 'Qual o primeiro nome da sua mãe?',
            question_type: name_mother_type,
            question_sub_type: QuestionSubType.where(question_type: name_mother_type, code: '1').take!
        },
        {
            description: 'Qual o último nome da sua mãe?',
            question_type: name_mother_type,
            question_sub_type: QuestionSubType.where(question_type: name_mother_type, code: '2').take!
        }
    ]
    questions.each do |question|
      Question.find_or_create_by!(question)
    end
  end
end
